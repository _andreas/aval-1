package utfpr.ct.dainf.if62c.avaliacao;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná DAINF - Departamento
 * Acadêmico de Informática IF62C - Fundamentos de Programação 2
 *
 * Primeira avaliação parcial 2014/2.
 *
 * @author a1363417
 */
public class Complexo {

    private double real;
    private double img;

    public Complexo() {
    }

    public Complexo(double real, double img) {
        // completar a implementação
        this.real = real;
        this.img = img;
    }

    // implementar getReal()
    public double getReal() {
        return real;
    }

    // implementar getImg() 
    public double getImg() {
        return img;
    }

    public Complexo soma(Complexo c) {
        return new Complexo(real + c.real, img + c.img);
    }

    // implementar sub(Complexo)
    public Complexo sub(Complexo c) {
        return new Complexo(real - c.real, img - c.img);
    }

    // implementar prod(double)
    public Complexo prod(double x) {
        return new Complexo(real * x, img * x);
    }

    // implementar prod(Complexo)
    public Complexo prod(Complexo c) {
        return new Complexo(real * c.real - img * c.img, img * c.real + real * c.img);
        // XY=(a1a2−b1b2)+(b1a2+a1b2)i
    }

    // implementar div(Complexo)
    public Complexo div(Complexo c) {
        return new Complexo((real * c.real + img * c.img) / (Math.pow(c.real, 2) + Math.pow(c.img, 2)), (img * c.real - real * c.img) / (Math.pow(c.real, 2) + Math.pow(c.img, 2)));
        //a1a2+b1b2 a2b1−a1b2
        //a2^2+b2^2 a2^2+b2^2
    }

    // implementar sqrt()
    public Complexo[] sqrt() {
        //double r = Math.sqrt(Math.pow(real, 2) + Math.pow(img, 2));
        //double p = Math.sqrt(r);
        double p = Math.sqrt(Math.sqrt(Math.pow(real, 2) + Math.pow(img, 2)));
        double fi = 0;
        Complexo[] result = new Complexo[2];

        if (real > 0) {
            fi = Math.atan(img / real);
        } else if (real < 0) {
            fi = Math.atan(img / real) + Math.PI;
        } else if (real == 0 && img == 0) {
            fi = 0;
        } else if (real == 0 && img > 0) {
            fi = (Math.PI) / 2.0;
        } else if (real == 0 && img < 0) {
            fi = (3.0 * Math.PI) / 2.0;
        }

        result[0] = new Complexo(p * Math.cos(fi / 2.0), p * Math.sin(fi / 2.0));
        result[1] = new Complexo(p * Math.cos(fi / 2.0 + Math.PI), p * Math.sin(fi / 2.0 + Math.PI));
        
        return result;
        // completar implementação
        // retornar o vetor contendo as raízes
        // √X={ρ(cosψ1+isenψ1),ρ(cosψ2+isenψ2)}
        // ρ=√r
        // ψ1=φ/2
        // ψ2=φ/2 + π     
        //return null;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + (int) (Double.doubleToLongBits(real)
                ^ (Double.doubleToLongBits(real) >>> 32));
        hash = 41 * hash + (int) (Double.doubleToLongBits(img)
                ^ (Double.doubleToLongBits(img) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final Complexo c = (Complexo) obj;
        return obj != null && getClass() == obj.getClass()
                && real == c.real && img == c.img;
    }

    @Override
    public String toString() {
        return String.format("%+f%+fi", real, img);
    }
}
