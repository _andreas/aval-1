
import utfpr.ct.dainf.if62c.avaliacao.Complexo;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná DAINF - Departamento
 * Acadêmico de Informática IF62C - Fundamentos de Programação 2
 *
 * Primeira avaliação parcial 2014/2.
 *
 * @author a1363417
 */
public class Avaliacao1 {

    public static void main(String[] args) {
        // implementar main
        Complexo[] result;
        Complexo a = new Complexo(1.0, 0);
        Complexo b = new Complexo(5.0, 0);
        Complexo c = new Complexo(4.0, 0);

        result = raizesEquacao(a, b, c);
        System.out.println("x1 = " + result[0]);
        System.out.println("x2 = " + result[1]);

        a = new Complexo(1.0, 0);
        b = new Complexo(2.0, 0);
        c = new Complexo(5.0, 0);

        result = raizesEquacao(a, b, c);
        System.out.println("y1 = " + result[0]);
        System.out.println("y2 = " + result[1]);
        // x^2+5x+4=0 e y^2+2y+5=0
    }

    // implementar raizesEquacao(Complexo, Complexo, Complexo)
    public static Complexo[] raizesEquacao(Complexo a, Complexo b, Complexo c) {
        Complexo[] result = new Complexo[2];
        Complexo[] teta = new Complexo[2];
        teta = ((b.prod(b)).sub((a.prod(c)).prod(4.0))).sqrt();

        result[0] = ((b.prod(-1.0)).soma(teta[0])).div(a.prod(2.0));
        result[1] = ((b.prod(-1.0)).soma(teta[1])).div(a.prod(2.0));
        return result;
        // −b+√b^2−4ac
        //     2a
    }

}
